const Web3 = require("web3")
const wsaddr = process.env.WS_PROVIDER_ADDR || "127.0.0.1:8545"
const httpaddr = process.env.HTTP_PROVIDER_ADDR || "127.0.0.1:8545"
const wsProvider = new Web3.providers.WebsocketProvider("ws://"+wsaddr);
const httpProvider = new Web3.providers.HttpProvider("http://"+httpaddr);
const web3 = new Web3(httpProvider)

web3.eth.net.isListening().then((s) => {
    console.log('We\'re still connected to the node');
}).catch((e) => {
    console.log('Lost connection to the node, reconnecting');
    console.log(e)
})