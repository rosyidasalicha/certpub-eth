'use strict';

const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../.env.docker') });

const Web3 = require("web3")
const wsaddr = process.env.WS_PROVIDER_ADDR || "127.0.0.1:8545"
const httpaddr = process.env.HTTP_PROVIDER_ADDR || "127.0.0.1:8545"
const wsProvider = new Web3.providers.WebsocketProvider("ws://"+wsaddr);
const httpProvider = new Web3.providers.HttpProvider("http://"+httpaddr);
const web3 = new Web3(wsProvider || httpProvider)

//console.log("Env : ", process.env.HTTP_PROVIDER_ADDR)

const eth = {}

/*
web3.eth.getBlockNumber(function (error, result) {
  console.log(result)
})

web3.eth.getAccounts(function (error, result) {
    console.log(result)
  })
*/

const { abi, networks } = require('../contract/CertificateBlockchain.json');
const privateKey = 'a47847a9da6a1d928856b5982e11ba56f147a955563d54c30d1f611bfbbcba21';
const account = web3.eth.accounts.privateKeyToAccount('0x' + privateKey);
web3.eth.accounts.wallet.add(account);
web3.eth.defaultAccount = account.address;

//const amountOfGas = contract.sendTokens.estimateGas(4, account);

//var contract = new  web3.eth.Contract(abi, networks["5777"].address)
var contract = new  web3.eth.Contract(abi, networks["1337"].address)

async function generateCertificate(id, param){
  try{
    var estimatedGas = await contract.methods.GenerateCertificate(id, param).estimateGas(
      {from: account.address}
    )
    try{
      var result = await contract.methods.GenerateCertificate(id, param).send({
        from: account.address,
        gas: estimatedGas
      })
      return id
    }
    catch(error){
      console.log("Contract call dailed with error: " + error.message);
      throw error
    }
  }
  catch(error){
    console.log("Failed estimatedGas with error: " + error.message);
    throw error
  }
}

async function getCertificate(id){
  try{
    var result = await contract.methods.GetData(id).call()
    console.log(result)
    return result
  }
  catch(error){
    console.log("Failed with error: " + error);
    throw error
  }
}


async function main(){
  await generateCertificate("a1", "Andi", "UB", "Blockchain Intro", 1729081261)
  await getCertificate("a1")
}

//main()
eth.contract = contract
eth.generateCertificate = generateCertificate
eth.getCertificate = getCertificate

module.exports = eth

