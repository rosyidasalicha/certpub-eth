'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CertData extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  CertData.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    id_sertifikat: DataTypes.STRING,
    nama_peserta: DataTypes.STRING,
    no_surat: DataTypes.STRING,
    unit_kerja: DataTypes.STRING

    // Off-chain
    // no_sertifikat, nama_peserta, judul, nama_kegiatan, user_id, user_type
  }, {
    sequelize,
    modelName: 'CertData',
  });
  return CertData;
};