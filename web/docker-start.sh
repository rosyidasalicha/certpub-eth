#!/bin/sh

# npm install
sequelize db:migrate
sequelize db:seed:all
# truffle migrate --network quorum

pm2-runtime app.js