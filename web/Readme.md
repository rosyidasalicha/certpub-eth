

### Create Certificate

POST /certificate

This route is used to create a new certificate. The request body should include the following fields:

no_sert: The certificate number. This field is required and should not be empty.
no_surat: The letter number. This field is required and should not be empty.
nama_peserta: The name of the certificate holder. This field is required and should not be empty.
nama_instansi: The name of the institution. This field is required and should not be empty.
id_instansi: The ID of the institution. This field is required and should contain a numeric value.
jenis_sertifikat: The type of certificate. This field is required and should not be empty.
kegiatan: The activity or event associated with the certificate. This field is required and should not be empty.
peran: The role of the certificate holder in the activity or event. This field is required and should not be empty.
predikat: The grade or rating of the certificate holder's performance. This field is required and should contain a numeric value.
tanggal_mulai: The start date of the activity or event. This field is required and should contain a date value.
tanggal_selesai: The end date of the activity or event. This field is required and should contain a date value.
tanggal_terbit: The date the certificate was issued. This field is required and should contain a date value.
nama_penandatangan: The name of the person signing the certificate. This field is required and should not be empty.
jabatan_penandatangan: The position of the person signing the certificate. This field is required and should not be empty.
nip_penandatangan: The NIP (National Identification Number) of the person signing the certificate. This field is required and should not be empty.
unit_kerja: The unit or department of the institution. This field is required and should not be empty.
kota_instansi: The city where the institution is located. This field is required and should not be empty.
prov_instansi: The province where the institution is located. This field is required and should not be empty.
aktif: Whether the certificate is active or not. This field is required and should not be empty.
tanggal_revoke: The date the certificate was revoked, if applicable. This field is optional and should contain a date value if provided.
user_type: The type of user creating the certificate. This field is required and should contain a numeric value.
The route will handle the request asynchronously, using the async keyword. The route will receive the request and response objects as arguments, which will be used to handle the request and send a response.

Once all required fields are provided and validated, the route will create a new certificate with the provided information. The response will include a success message and the newly created certificate. If any of the required fields are missing or invalid, the route will return an error message.