const express = require('express')
const eth = require('./eth')
const passport = require('passport');
const db = require("./models")
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '/.env.docker') });

require('./auth/auth');

const app = express()
const port = 8080

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/auth-routes');
const secureRoute = require('./routes/secure-routes');

app.use(passport.initialize())
app.use('/auth', routes)
app.use('/', passport.authenticate('jwt', { session: false }), secureRoute)

eth.contract.events.CertificateGenerated(async (error, event) => {
    if(error == null){
        const values = event.returnValues
        const data = await db.CertData.create({
            id_sertifikat: values.id_sertifikat,
            nama_peserta: values.nama_peserta,
            no_surat: values.no_surat,
            unit_kerja: values.unit_kerja
        })
        //console.log(values)
    }
})

/*
// Get past events
contract.getPastEvents('CertificateGenerated', {})
    .then(results => console.log(results))
    .catch(err => throw err);
*/

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
