const express = require('express')
const eth = require('../eth')
const { body, validationResult } = require('express-validator');
const crypto = require('crypto');
const { DateTime } = require("luxon");

const router = express.Router();

router.post("/test", async (req,res) => {
    return res.json({"msg" : "OK", "user": req.user})
})

router.get('/certificate/:certId', async (req, res) =>{
    try{
        const data = await eth.getCertificate(req.params.certId)
        let tanggalRevoke = ""
        if(data.detail.tanggal_revoke != 0){
            tanggalRevoke = DateTime.fromMillis(data.detail.tanggal_revoke * 1000).toISODate()
        }
        const returnData = {
            no_sert: data.no_sert,
            no_surat: data.no_surat,
            nama_peserta: data.nama_peserta,
            nama_instansi: data.nama_instansi,
            id_instansi: data.id_instansi,
            jenis_sertifikat: data.jenis_sertifikat,
            kegiatan: data.kegiatan,
            peran: data.peran,
            predikat: data.predikat,
            tanggal_mulai: DateTime.fromMillis(data.detail.tanggal_mulai * 1000).toISODate(),
            tanggal_selesai: DateTime.fromMillis(data.detail.tanggal_selesai * 1000).toISODate(),
            tanggal_terbit: DateTime.fromMillis(data.detail.tanggal_terbit * 1000).toISODate(),
            nama_penandatangan: data.detail.nama_penandatangan,
            jabatan_penandatangan: data.detail.jabatan_penandatangan,
            nip_penandatangan: data.detail.nip_penandatangan,
            unit_kerja: data.detail.unit_kerja,
            kota_instansi: data.detail.kota_instansi,
            prov_instansi: data.detail.prov_instansi,
            aktif: data.detail.aktif,
            tanggal_revoke: tanggalRevoke,
            tambahan: data.detail.tambahan,
            user_type: data.detail.user_type
        }
        res.json(returnData)
    }
    catch(err){
        if(err.message == "Returned error: VM Exception while processing transaction: revert No data exists"){
            res.status(404)
            res.json(err.message)
        }
        else{
            res.status(500)
            res.json(err.message)
        }
        console.log(err.code)
    }
})

router.post('/certificate', 
    body('no_sert').notEmpty(),
    body('no_surat').notEmpty(),
    body('nama_peserta').notEmpty(),
    body('nama_instansi').notEmpty(),
    body('id_instansi').isNumeric(),
    body('jenis_sertifikat').notEmpty(),
    body('kegiatan').notEmpty(),
    body('peran').notEmpty(),
    body('predikat').isNumeric(),
    body('tanggal_mulai').isDate(),
    body('tanggal_selesai').isDate(),
    body('tanggal_terbit').isDate(),
    body('nama_penandatangan').notEmpty(),
    body('jabatan_penandatangan').notEmpty(),
    body('nip_penandatangan').notEmpty(),
    body('unit_kerja').notEmpty(),
    body('kota_instansi').notEmpty(),
    body('prov_instansi').notEmpty(),
    body('aktif').notEmpty(),
    body('tanggal_revoke').optional().isDate(),
    body('user_type').isNumeric(),
    async (req,res) => {
        const valError = validationResult(req);
        if (!valError.isEmpty()) {
            return res.status(400).json({ error: valError.array() });
        }
        let hash = crypto.createHash('md5').update(JSON.stringify(req.body)).digest('hex');
        try{
            let tanggalRevoke = 0
            if(req.body.tanggal_revoke != undefined){
                tanggalRevoke = Math.round(new Date(req.body.tanggal_revoke).getTime()/1000)
            }
            const param = {
                no_sert: req.body.no_sert,
                no_surat: req.body.no_surat,
                nama_peserta : req.body.nama_peserta,
                nama_instansi: req.body.nama_instansi,
                id_instansi: req.body.id_instansi,
                jenis_sertifikat : req.body.jenis_sertifikat,
                kegiatan: req.body.kegiatan,
                peran: req.body.peran,
                predikat : req.body.predikat,
                detail : {
                    tanggal_mulai: Math.round(new Date(req.body.tanggal_mulai).getTime()/1000),
                    tanggal_selesai: Math.round(new Date(req.body.tanggal_selesai).getTime()/1000),
                    tanggal_terbit : Math.round(new Date(req.body.tanggal_terbit).getTime()/1000),
                    nama_penandatangan: req.body.nama_penandatangan,
                    jabatan_penandatangan: req.body.jabatan_penandatangan,
                    nip_penandatangan : req.body.nip_penandatangan,
                    unit_kerja: req.body.unit_kerja,
                    kota_instansi: req.body.kota_instansi,
                    prov_instansi : req.body.prov_instansi,
                    aktif: req.body.aktif,
                    tanggal_revoke: tanggalRevoke,
                    tambahan : req.body.tambahan,
                    user_id: req.body.user_id,
                    user_type : req.body.user_type
                } 
            }
            const result = await eth.generateCertificate( hash, param)
            return res.json({"message": "Succesfully created "+result})
        }
        catch(err){
            res.status(500)
            console.log(err.message)
            return res.json({error: err.message})
        }    
})

module.exports = router;