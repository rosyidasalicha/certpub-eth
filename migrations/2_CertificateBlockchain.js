const CertificateBlockchain = artifacts.require("CertificateBlockchain");

module.exports = function (deployer) {
  deployer.deploy(CertificateBlockchain, "Certificate Blockchain");
};