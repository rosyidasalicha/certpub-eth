// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract CertificateBlockchain {
  //constructor() public {}

  struct Certificate {
      string no_sert;
      string no_surat;
      string nama_peserta;
      string nama_instansi;
      uint32 id_instansi;
      string jenis_sertifikat;
      string kegiatan;
      string peran;
      uint8 predikat;
      CertificateDetail detail;
  }

  struct CertificateDetail{
      uint32 tanggal_mulai;
      uint32 tanggal_selesai;
      uint32 tanggal_terbit;
      string nama_penandatangan;
      string jabatan_penandatangan;
      string nip_penandatangan;
      string unit_kerja;
      string kota_instansi;
      string prov_instansi;
      bool aktif;
      uint32 tanggal_revoke;
      string tambahan;
      uint32 user_id;
      uint8 user_type;
  }
  
  mapping(string => Certificate) public certificates;

  event CertificateGenerated(string id_sertifikat, string nama_peserta, string no_surat, string unit_kerja);

  function GenerateCertificate(
      string memory id_sert, Certificate memory certData) public returns (string memory cert_id) { //
      require(certificates[id_sert].id_instansi == 0, "Data already exist");
      certificates[id_sert] = certData; // user_id = 0
      emit CertificateGenerated(id_sert, certificates[id_sert].nama_peserta, certificates[id_sert].no_surat, certificates[id_sert].detail.unit_kerja );
      return id_sert;
  }

  function GetData(string memory _id) public view returns(Certificate memory cert) {
      Certificate memory temp = certificates[_id];
      require(temp.id_instansi != 0, "No data exists"); // expiration_date = tanggal_revoke
      return temp;
  }
}
